# Archives

Archive of old public mailing lists:
- Spi-announce: Software in the Public Interest announcements
- Spi-general: General discussions related to Software in the Public Interest
- Spi-bylaws: By-laws revision committee discussions (inactive since January 2012)
- Spi-www: SPI web-server related discussions (inactive since October 2011)

After uncompressing, archives are in mailman archive format, i.e. they can be read using a browser, are sorted by month and can be view by thread, subject, author or date. Raw mbox can also be downloaded.

Used for uploading to https://lists-old.spi-inc.org
